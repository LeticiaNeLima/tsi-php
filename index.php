<?php


ini_set('display_errors', 1);

ini_set('display_startup_errors', 1);

error_reporting(E_ALL);


//como imprime na tela 
echo 'Olá Mundo <br><br>';


//variáveis
$nome = 'Letícia Higino Neri Lima';

echo 'olá, $nome ! <br><br>';

echo 'olá, ' . $nome . '! <br><br>'; //concatenando

echo "olá, $nome  <br><br>";


//estrutura condicional
if ($nome == 'Letícia'){
    echo 'o nome é igual  <br><br>';
}else{
    echo 'o nome não é igual  <br><br>';
}

//outra condicional 
$dia = 'sexta';

switch($dia){

    case 'segunda';

        echo 'Estude';
    break;

    case 'terça';
    
        echo 'vá para aula de CMS';
    break;

    case 'quarta';
    
    echo 'vá para aula de BD';
    break;

    case 'quinta';
    
    echo 'venha para cá';
    break;

    case 'sexta';
    
    echo 'vá para algum outro lugar ';
    break;

    default:

        echo 'vá descansar';

}

echo '<br><br>';
echo '<br><br>';


//ternário
$animal = 'cachorro';

$tipo = $animal == 'cachorro' ? 'mamífero' : 'desconhecido';



echo "<br> $animal é do tipo : $tipo";

echo '<br><br>';
echo '<br><br>';

$sobrenome_informado = 'Lima';
$sobrenome = $sobrenome_informado ?? 'não informado';
echo "<br> sobrenome: $sobrenome <br>";



//estrututra de repetição. Quando tem contador 
echo'<br>for<br>';
for($i = 0; $i < 10; $i++){
    echo 'essa linha é '  . $i . '<br>';
}


// quando não tem contador 
echo'<br>while<br>';
$i = 0;
while($i < 10){
    echo 'essa linha é '  . $i . '<br>';

    $i++;
}


echo'<br> do..while<br>';
$i = 0;
do{
    echo 'essa linha é '  . $i . '<br>';
    $i++;

}while($i < 10);


//COMO CHAMAR OUTROS CÓDIGOS 

include 'link.html'; // se não existir link.html dá um erro mas continua a execução

require 'link.html'; // se não existir link.html dá um erro fatal e para o programa

include_once 'link.html'; //verifica se já foi incluido antes, se sim, não inclui novamente 

require_once 'link.html'; //verifica se já foi incluido antes, se sim, não inclui novamente 