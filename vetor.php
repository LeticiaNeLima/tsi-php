<?php


$despesas[0] = 345.55;
$despesas[1] = 135.00;
$despesas[2] = 600.00;
$despesas[3] = 900.00;
$despesas[4] = 400.00;

for($i = 0; $i < 5; $i++){
    echo $despesas[$i] . "<br> ";
}

unset($despesas); //apaguei o vetor
          //$indice   //$valor 
$despesas['mercado'] = 345.55;
$despesas['estacionamento'] = 135.00;
$despesas['alimentação'] = 600.00;
$despesas['bar'] = 900.00;
$despesas['educacao'] = 400.00;

echo '<br> Despesas: <br>';
//looping para vetor 
foreach ($despesas as $indice => $valor){
     
    echo " $indice: R$" . number_format($valor, 2, ',', '.') ." <br>"; 
    
}

echo '<br> Exercíco do dia: <br>';

$dia['Segunda'] = "estudo e faço estágio";
$dia['Terça'] = "estudo e faço estágio";
$dia['Quarta'] = "estudo e faço estágio";
$dia['Quinta'] = "estudo e faço estágio";
$dia['Sexta'] = "estudo e faço estágio";
$dia['Sábado'] = "descanso e brinco com meus cahorros";
$dia['Domingo'] = "descanso e brinco com meus cahorros";

foreach ($dia as $indice => $valor){
     
    echo " $indice eu $valor<br>"; 
}